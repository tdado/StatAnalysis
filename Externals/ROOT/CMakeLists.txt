# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building ROOT as part of the statistics release.
#

if (NOT ATLAS_BUILD_ROOT )
   return()
endif()

# Set the name of the package:
atlas_subdir( ROOT )

message(STATUS "BUILDING: ROOT ${STATANA_ROOT_VERSION} as part of release")

# Temporary directory for the build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/ROOTBuild )

# Extra arguments for the CMake configuration of the ROOT build:
set( _extraArgs )
if( APPLE )
   list( APPEND _extraArgs -Dvc:BOOL=OFF -Drpath:BOOL=ON )
endif()

list( APPEND _extraArgs -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )

# Set the C++ standard for the build.
if( NOT "${CMAKE_CXX_STANDARD}" STREQUAL "" )
   list( APPEND _extraArgs -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD}
      -DCMAKE_CXX_EXTENSIONS:BOOL=${CMAKE_CXX_EXTENSIONS} )
endif()

# The build needs Python:

if( ATLAS_BUILD_PYTHON )
   # version string needs updating as Python package in atlasexternals moves to newer versions (e.g. from 3.7 to 3.8)
   list( APPEND _extraArgs
      -DPYTHON_EXECUTABLE:FILEPATH=${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/python3
      -DPython3_EXECUTABLE:FILEPATH=${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/python3
      -DPython3_INCLUDE_DIR:PATH=${CMAKE_INCLUDE_OUTPUT_DIRECTORY}/python3.10
      -DPython3_LIBRARY:PATH=${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/${CMAKE_SHARED_LIBRARY_PREFIX}python3.10${CMAKE_SHARED_LIBRARY_SUFFIX} )
else()
   find_package( Python COMPONENTS Interpreter Development REQUIRED )
   list( APPEND _extraArgs -DPYTHON_EXECUTABLE:PATH=${Python_EXECUTABLE} )
   if( "${Python_VERSION}" VERSION_LESS 3 )
      list( APPEND _extraArgs
         -DPython2_EXECUTABLE:FILEPATH=${Python_EXECUTABLE}
         -DPython2_INCLUDE_DIR:PATH=${Python_INCLUDE_DIRS}
         -DPython2_LIBRARY:PATH=${Python_LIBRARIES} )
   else()
      list( APPEND _extraArgs
         -DPython3_EXECUTABLE:FILEPATH=${Python_EXECUTABLE}
         -DPython3_INCLUDE_DIR:PATH=${Python_INCLUDE_DIRS}
         -DPython3_LIBRARY:PATH=${Python_LIBRARIES} )
   endif()
endif()

# ...and TBB.
#if( ATLAS_BUILD_TBB )
#   list( APPEND _extraArgs
#      -DTBB_ROOT_DIR:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM} )
#else()
#   find_package( TBB REQUIRED )
#   get_filename_component( TBB_ROOT_DIR ${TBB_INCLUDE_DIR} DIRECTORY )
#   list( APPEND _extraArgs -DTBB_ROOT_DIR:PATH=${TBB_ROOT_DIR} )
#endif()

# ...and XRootD. Note that if we are not building XRootD ourselves, we
# just leave it up to ROOT to find it the best that it can.
if( ATLAS_BUILD_XROOTD )
   list( APPEND _extraArgs
      -Dxrootd:BOOL=ON
      -DXROOTD_ROOT_DIR:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM} )
endif()

# ...and optionally DCAP.
if( ATLAS_BUILD_DCAP )
   list( APPEND _extraArgs
      -Ddcache:BOOL=ON -DDCAP_DIR:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM} )
endif()

# ...and optionally Davix.
if( ATLAS_BUILD_DAVIX )
   list( APPEND _extraArgs -Ddavix:BOOL=ON )
endif()

# ...and optionally LibXml2.
if( ATLAS_BUILD_LIBXML2 OR ATLAS_BUILD_DAVIX )
   list( APPEND _extraArgs
      -DCMAKE_PREFIX_PATH:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM} )
endif()

# ...and optionally FFTW3.
if( "${CMAKE_SYSTEM_PROCESSOR}" STREQUAL "x86_64" OR
    "${CMAKE_SYSTEM_PROCESSOR}" STREQUAL "i686" )
   list( APPEND _extraArgs -Dbuiltin_fftw3:BOOL=ON )
endif()


# Generate the script that will configure the build of ROOT.
string( REPLACE ";" " " _configureArgs "${_extraArgs}" )
configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/configure.sh.in
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure_pregen.sh
   @ONLY )
file( GENERATE
   OUTPUT ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh
   INPUT ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure_pregen.sh )
unset( _configureArgs )

# The source and version of ROOT to build.
set( ATLAS_ROOT_SOURCE "GIT_REPOSITORY;https://github.com/root-project/root.git"
        CACHE STRING "Source for the ROOT build" )
set( ATLAS_ROOT_VERSION "GIT_TAG;${STATANA_ROOT_VERSION}"
        CACHE STRING "The 'version' of the ROOT source for its build" )
mark_as_advanced( ATLAS_ROOT_SOURCE ATLAS_ROOT_VERSION )


# Build ROOT:
ExternalProject_Add( ROOT
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   SOURCE_DIR ${CMAKE_BINARY_DIR}/src/ROOT
   BUILD_ALWAYS ${TRACK_CHANGES_ROOT}
   LOG_DOWNLOAD ON LOG_CONFIGURE ON LOG_BUILD OFF LOG_INSTALL ON LOG_OUTPUT_ON_FAILURE ON
   ${ATLAS_ROOT_SOURCE}
   ${ATLAS_ROOT_VERSION}
   CONFIGURE_COMMAND
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh
   CMAKE_CACHE_ARGS
   -Dccache:BOOL=${CCACHE}
   UPDATE_COMMAND "" # needed for next line to work
   UPDATE_DISCONNECTED TRUE) # skips reconfigure+build if just rerunning. Note: need to remove src/ROOT-stamp dir to recheckout code
ExternalProject_Add_Step( ROOT forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "Forcing the re-download of ROOT"
   DEPENDERS download )
ExternalProject_Add_Step( ROOT purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for ROOT"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( ROOT buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR>
   COMMENT "Installing ROOT into the build area"
   DEPENDEES install )
add_dependencies( Package_ROOT ROOT )
if( ATLAS_BUILD_PYANALYSIS )
   add_dependencies( ROOT Package_PyAnalysis )
endif()
if( ATLAS_BUILD_PYTHON )
   add_dependencies( ROOT Python )
endif()
if( ATLAS_BUILD_TBB )
   add_dependencies( ROOT TBB )
endif()
if( ATLAS_BUILD_XROOTD )
   add_dependencies( ROOT XRootD )
endif()
if( ATLAS_BUILD_DCAP )
   add_dependencies( ROOT dcap )
endif()
if( ATLAS_BUILD_DAVIX )
   add_dependencies( ROOT Davix )
endif()
if( ATLAS_BUILD_LIBXML2 )
   add_dependencies( ROOT LibXml2 )
endif()

# Install ROOT:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
