# Pick up bash tests
file(GLOB TestScripts "${CMAKE_CURRENT_SOURCE_DIR}/tests/*/*.sh")

foreach(TestScript ${TestScripts})
  get_filename_component(TestName ${TestScript} NAME)
  get_filename_component(TestDir ${TestScript} DIRECTORY)
  get_filename_component(TestDir ${TestDir} NAME)
  string(TOUPPER ${TestDir} TestDir)
  # todo: this check requires the cmake cache variables to be moved somewhere so they can be loaded here
  if( (NOT DEFINED ${STATANA_${TestDir}_VERSION}) OR (NOT ${STATANA_${TestDir}_VERSION} STREQUAL ""))
    add_test(NAME ${TestName} WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} COMMAND bash -e ${TestScript})
    set_tests_properties(${TestName} PROPERTIES ENVIRONMENT "PYTHONPATH=$ENV{PYTHONPATH}:${EXPORT_PYTHONPATH};LD_LIBRARY_PATH=$ENV{LD_LIBRARY_PATH}:${EXPORT_LD_LIBRARY_PATH}")
  else()
    message(STATUS "Skipping test " ${TestScript})
  endif()
endforeach()


enable_testing()
